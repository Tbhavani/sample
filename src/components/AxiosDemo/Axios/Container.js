import React from 'react'
import Presentation from './Presentation'

class Container extends React.Component
{
    constructor(){
        super()
        this.state = {
            count : 0,
            name :'',
            isLight : false,
            studentList : []
        }
    }

    componentDidMount()
    {
        this.setState({
            studentList : [
                {
                    htno : "19K1A0532",
                    name : "Pratyusha",
                    year : "II-I"
                },
                {
                    htno : "19K1A0501",
                    name : "Sravani",
                    year : "II-I"
                },
                {
                    htno : "19K1A0549",
                    name : "Yeswanth",
                    year : "II-I"
                },
                {
                    htno : "19K1A05h0",
                    name : "Sudheer",
                    year : "II-I"
                },
                           ]
        })
    }

    incrHandler = () => {
        this.setState({
            count : this.state.count+1
        })
    }

    decrHandler = () => {
        this.setState({
            count : this.state.count-1
        })
    }

    setNameHandler = (e) => {
        this.setState({
            name : e.target.value
        })
    }
    lightHandler= () => {
        this.setState({
            isLight : this.state.isLight ? false : true
        },()=>{
            // console.log(this.state.isLight)
        })
    }
    render(){
        return(
            <Presentation  
            {...this.state}
            lightHandler={this.lightHandler}
            incrHandler = {this.incrHandler}
            decrHandler = {this.decrHandler}
            setNameHandler = {this.setNameHandler}
            />
        )
    }
}

export default Container