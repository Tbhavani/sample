const Presentation = ({name,count,isLight,incrHandler,decrHandler,lightHandler,setNameHandler, studentList}) => {
    // const [name,count,isLight,incrHandler,decrHandler,lightHandler,setNameHandler] = props

    return(
        <div>
            <h4>The count is : {count} </h4>
            
            <button onClick={incrHandler}>Increment</button>
            <button onClick={decrHandler}>Decrement</button><br/>

            Enter Your Name : <input type="text" value={name} onChange={setNameHandler}></input>
            {
                name.length >0  && <div>Thank you {name}</div>
            }
            {console.log(isLight)}
            <button onClick={lightHandler}>
                {
                    isLight ? 'OFF' : 'ON'
                }
            </button>
            { isLight
                ?
                <img src="/images/download.jpg" alt="" />
                :
                <img src="/images/bulb off.jpg" alt="" />
            }
            <table border="1" align="center">
            {
                studentList.map((student,index) => {
                    return(
                        <tr>
                            <td>{student.htno}</td>
                            <td>{student.name}</td>
                            <td>{student.year}</td>
                        </tr>
                    )
                })
            }
            </table>
        </div>
    )
}
export default Presentation