const MyTextInput = () => {
    return(
        <input 
        type="text"
         value={props.name}
         validate={props.validate}
        />
    )
}
export default MyTextInput