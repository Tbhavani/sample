import React from 'react';
class Classdemo1 extends React.Component{
    render(){
        return(
            <div>
                <h1>welcome to class Components - {this.props.name} - {this.props.age}</h1>
            </div>
        )
    }
}
export default Classdemo1                                                          