import React from 'react'
import Presentation from './presentation'
class Container extends React.Component{
    constructor(){
        super()
        count : 0
    }
    setCounthandler = () => {
        this.setState({
            count : this.state.count+1
        })
    }
    render(){
        return(
            <Presentation
            count ={this.state.count}
            setCounthandler  = {this.setCounthandler}
            />
        )
    }
}
export default Container