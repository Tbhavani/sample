import React from 'react'
class StateClassDemo extends React.Component{
    constructor(){
        super()
        this.state = {
            count : 0
        }
    }
    clickHandler= () => {
        this.setState({
            count : this.state.count+1
        })
    }
    render(){
        return(
            <div>
                <button onClick={this.clickHandler} >Clicked : {this.state.count} </button>
            </div>
        )
    }
}
export default StateClassDemo