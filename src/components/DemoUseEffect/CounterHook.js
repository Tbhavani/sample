import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';


const CounterHook = () => {
    const [count, setcount] = useState(0);
    const [name, setname] = useState("");
    useEffect(() => {
        console.log("Executing Useeffect")
        document.title = `You clicked ${count} times`;
    },[count]);
    return (
        <div>
            <TextField 
            id="outlined-basic" 
            label="Name" 
            variant="outlined" 
            value = {name} 
            onChange={(e) => setname(e.target.value)}/>{name}
            <Typography variant="h2" gutterBottom>
                The count is : {count}
            </Typography>
            <Button variant="contained" color="primary" onClick={() => setcount(count + 1)}>
                IncreamentCount
</Button>

        </div>
    );
}
export default CounterHook