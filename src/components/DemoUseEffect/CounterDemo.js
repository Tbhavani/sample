import React from 'react'


class CounterDemo extends React.Component{
    constructor(){
        super()
        this.state = {
            count : 0
        }
    }
    componentDidMount(){
        document.title = `You clicked ${this.state.count} times`;
    }
    componentDidUpdate(preProps, preStates){
        document.title = `You clicked ${this.state.count} times`;
    }
    countHandler = () => {
        this.setState({
            count : this.state.count+1
        })
    }
    render(){
        return(
            <div>
                The count is : {this.state.count}
                <button onClick ={this.countHandler}>IncreamentCount</button>
            </div>
        )

    }
}
export default CounterDemo