import React, { useState } from 'react'

const StateArrayDemo = () => {
    const [name, setname] = useState('')
    const [items, setitems] = useState([])
    const addColor=()
    return(
        <div>
        Name : <input type="text" onChange={(e)=> setname(e.target.value)}/>
        <button onClick={addColor}>Add</button>
            <h1> list of colours </h1>
            <ul>
                {
                    items.map(item =>{
                        <li>key={item.id}>{item.name}</li>
                    })
                }
            </ul>
        </div>
    )
}

    
