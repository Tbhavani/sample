import React , {useState} from 'react'
import TextField from '@material-ui/core/TextField';

const StateObjectDemo = () => {
    const [name,setName] = useState({firstname :'', lastname: ''})
    return(
        <div>
        
            <TextField id="standard-basic" label="FirstName" onChange={(e)=> setName({...name, firstname: e.target.value})} />{name.firstname}<br/>
            <TextField id="standard-basic" label="LastName" onChange={(e)=> setName({...name, lastname: e.target.value})} />{name.lastname}<br/>
            <TextField id="standard-basic" label="regno" onChange={(e)=> setName({...name, regno: e.target.value})} />{name.regno}<br/>
            <TextField id="standard-basic" label="Department" onChange={(e)=> setName({...name, Department: e.target.value})} />{name.Department}<br/>
            <TextField id="standard-basic" label="PhoneNumber" onChange={(e)=> setName({...name, PhoneNumber: e.target.value})} />{name.PhoneNumber}<br/>
            <TextField id="standard-basic" label="Emailid" onChange={(e)=> setName({...name, Emailid: e.target.value})} />{name.EmailId}<br/>
             {JSON.stringify(name)}
        </div>
    )
}
export default StateObjectDemo