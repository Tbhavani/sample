import React , {useState} from 'react'

const StateHooksDemo = () => {
    const [count,setCount] = useState(0)
    const incrByTen= () => {
        for(let i=0;i<10;i++)
            setCount(preCount => preCount+2)
    }
    return(
        <div>
            <button onClick={() => setCount(preCount=> preCount+2)} > Clicked : {count}</button>
            <button onClick={incrByTen} > Incr By 10 : {count}</button>
        </div>
    )
}

export default StateHooksDemo