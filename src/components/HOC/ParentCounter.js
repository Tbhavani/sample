import React from 'react'
import CounterHead from './CounterHead'
import CounterDemo from './CounterDemo'
class ParentCounter extends React.Component{
    constructor(){
        super()
        this.state = {
            count : 0
        }
    }
    clickhandler = () =>{
     this.setState({
         count : this.state.count+1
     })
    }
    render(){
        return(
            <div>
                <CounterHead count = {this.state.count} clickhandler = {this.clickhandler}/>
                <CounterHead count = {this.state.count} clickhandler = {this.clickhandler}/>    
                               </div>
        )
    }

}
}