import React  from 'react'
class CounterDemo extends React.Component{
    constructor(){
        super()
        this.state = {
            count : 0
        }
    }
    clickHandler = () => {
     this.setState({
         count : this.state.count+1
     })
    }
    render(){
        return(
            <div>
                <button onClick ={this.clickHandler}> Counter : {this.state.count}</button>
            </div>
        )
    }
}
export default CounterDemo