import React from 'react'

class CounterHead extends React.Component{
    constructor(){
        super()
        this.state = {
            count : 0
        }
    }
    clickhandler = () =>{
     this.setState({
         count : this.state.count+1
     })
    }
    render(){
        return(
            <div>
                <h1 onMouseOver={this.clickhandler}>Welcome to HOC components-Counter : {this.state.count}</h1>
                </div>
        )
    }

}
export default CounterHead