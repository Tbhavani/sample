import React from 'react';
class Events extends React.Component{
    constructor(){
        super()
        this.state = {
            count : 0,
            headCount : 0
        }
    }
    countHandler = () => {
        this.setState({
            count : this.state.count+1
        })
    
    }
    headCountHandler = () => {
        this.setState({
            headCount : this.state.headCount+1
        })
    }
    render(){
        return(
            <div>
            <h1 onMouseOver={this.headCountHandler}>Welcome to React designing - visisted : {this.state.headCount}</h1>
                <h2>The count is : {this.state.count}</h2>
               <input type="button" value="Counter" onClick={this.countHandler}/>
            </div>
        )
    }
}
export default Events