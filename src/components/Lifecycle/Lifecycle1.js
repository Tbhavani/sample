import React from 'react';
class Lifecycle1 extends React.Component{
    constructor(){
        super()
        this.state={
            name : 'yeswanth',
            age : '20'
        }
    }
    render(){
        return(
            <div>
                <h1> hai {this.state.name}</h1>
                <h4>Age {this.state.age}</h4>
            </div>
        )
    }
}
export default Lifecycle1
