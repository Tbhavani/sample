const Presentation = ({ isLight, lightHandler, image }) => {

    return (
        <div>


            { isLight
                ?
                <img src="/images/download.jpg" alt="" />
                :
                <img src="/images/bulb off.jpg" alt="" />
            }
            <button onClick={lightHandler}>
                {
                    isLight ? 'OFF' : 'ON'
                }
            </button>
        </div>
    )
}
export default Presentation
