import React from 'react'
import Presentation from './Presentation'
class Container extends React.Component
{
    constructor(){
        super();
        this.state = {
            isLight : false,
        }
    }

    lightHandler= () => {
        this.setState({
            isLight : this.state.isLight ? false: true,
        })
    }
    render(){
        return(
            <Presentation  
            {...this.state}
            lightHandler = {this.lightHandler}
            />
        )
    }
}

export default Container;