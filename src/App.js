import React from 'react';
import './App.css';
import StateClassDemo from './components/demo/StateClassDemo';
import CounterDemo from './components/DemoUseEffect/CounterDemo'
import CounterHook from './components/DemoUseEffect/CounterHook'
//import  Alert from '@material-ui/lab/Alert';
//import Container from './components/counter/Container';
//import CounterDemo from './components/HOC/CounterDemo'
//import CounterHead from './components/HOC/CounterHead'
//import Classdemo1 from './components/Classdemo1'
//import Democomp from './components/Democomp'
//import Democomp1 from './components/Democomp1'
//import Funcomp1 from './components/funcomp1'
//import Events from './components/Events/Events'
//import Events1 from './components/Events/Events1'
//import Lifecycle1 from './components/Lifecycle/Lifecycle1'
//import StateHooksDemo from './components/hooks/StateHooksDemo'
//import StateObjectDemo from './components/hooks/StateObjectDemo'
//import Container from './components/AxiosDemo/Axios/Container'


function App ()
{
  
  return (

    <div
    className = "App">
     <h1>WELCOME TO REACT DESIGNING</h1>
     <StateClassDemo/>
     <CounterDemo/>
     <CounterHook/>
    </div>
  );
}
  export default App
